package com.epam.spring.security.service;

import com.epam.spring.security.model.Team;
import com.epam.spring.security.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    @PostFilter("hasRole('ADMIN') or filterObject.country.startsWith('S')")
    public List<Team> findAllFiltered() {
        return teamRepository.findAll();
    }

    @PostAuthorize("hasRole('ADMIN') or returnObject.country == 'England'")
    public Team findById(Integer id) {
        return teamRepository.findById(id);
    }

    @PreAuthorize("hasRole('ADMIN') or #name == 'Dynamo' or (authentication.principal.username=='user2' and #country!='Spain')")
    public void add(String name, String country) {
        teamRepository.add(name, country);
    }

    @PreFilter("hasRole('ADMIN') or filterObject.country == 'Ukraine'")
    public void addAll(List<Team> teams) {
        teamRepository.addAll(teams);
    }

    @Secured("ROLE_ADMIN")
    public void delete(int id) {
        teamRepository.delete(id);
    }

}
