package com.epam.spring.security.controller;


import com.epam.spring.security.model.Team;
import com.epam.spring.security.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Team>> findAll() {
        return ResponseEntity.ok().body(teamService.findAll());
    }

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public ResponseEntity<List<Team>> findAllFiltered() {
        return ResponseEntity.ok().body(teamService.findAllFiltered());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Team> findById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok().body(teamService.findById(id));
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<List<Team>> add(@RequestParam("name") String name, @RequestParam("country") String country) {
        teamService.add(name, country);

        return ResponseEntity.ok().body(teamService.findAll());
    }

    @RequestMapping(value = "/addAll", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<List<Team>> addAll(@RequestBody List<Team> teams) {
        teamService.addAll(teams);

        return ResponseEntity.ok().body(teamService.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<List<Team>> delete(@PathVariable(value = "id") Integer id) {
        teamService.delete(id);

        return ResponseEntity.ok().body(teamService.findAll());
    }

}


