package com.epam.spring.security.repository;

import com.epam.spring.security.model.Team;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TeamRepository {

    private Map<Integer, Team> teams;
    private Integer id = 0;

    public TeamRepository() {
        teams = new HashMap<>();
        teams.put(++id, new Team(id, "Real Madrid", "Spain"));
        teams.put(++id, new Team(id, "Manchester United", "England"));
        teams.put(++id, new Team(id, "Juventus", "Italy"));
    }

    public List<Team> findAll() {
        return new ArrayList<>(teams.values());
    }

    public Team findById(Integer id) {
        return teams.get(id);
    }

    public void add(String name, String country) {
        teams.put(++id, new Team(id, name, country));
    }

    public void addAll(List<Team> teams) {
        teams.stream().forEach(t -> this.teams.put(++id, new Team(id, t.getName(), t.getCountry())));
    }

    public void delete(int id) {
        teams.remove(id);
    }


}
