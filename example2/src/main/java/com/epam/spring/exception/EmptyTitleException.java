package com.epam.spring.exception;

public class EmptyTitleException extends RuntimeException {

    public EmptyTitleException(String message) {
        super(message);
    }
}
