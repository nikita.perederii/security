package com.epam.spring.repository;

import com.epam.spring.model.Recipe;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RecipeRepository {

    private Map<Integer, Recipe> recipes;

    public RecipeRepository() {
        recipes = new HashMap<>();
        recipes.put(1, new Recipe("Pizza", 1));
        recipes.put(2, new Recipe("Salad", 2));
        recipes.put(3, new Recipe("Sandwich", 3));
    }

    public List<Recipe> findAll() {
        return new ArrayList<>(recipes.values());
    }

    public void add(Recipe recipe) {
        recipes.put(recipe.getId(), recipe);
    }

    public void delete(int id) {
        recipes.remove(id);
    }
}
