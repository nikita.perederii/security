package com.epam.spring.controller;

import com.epam.spring.model.Recipe;
import com.epam.spring.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homepage(Model model) {
        return "homepage";
    }

    @RequestMapping(value = "/anonymous", method = RequestMethod.GET)
    public String anonymous(Model model) {
        return "anonymous";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        return "login";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String user(Model model) {
        model.addAttribute("recipes", recipeService.getAll());

        return "user";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin(Model model) {
        model.addAttribute("recipes", recipeService.getAll());
        model.addAttribute("recipe", new Recipe());

        return "admin";
    }

    @RequestMapping(value = "/addRecipe", method = RequestMethod.POST)
    public String addRecipe(@ModelAttribute Recipe recipe, Model model) {
        recipeService.add(recipe);
        model.addAttribute("recipe", new Recipe());
        model.addAttribute("recipes", recipeService.getAll());

        return "admin";
    }

    @RequestMapping(value = "/deleteRecipe", method = RequestMethod.POST)
    public String deleteRecipe(@ModelAttribute Recipe recipe, Model model) {
        recipeService.delete(recipe.getId());
        model.addAttribute("recipe", new Recipe());
        model.addAttribute("recipes", recipeService.getAll());

        return "admin";
    }
}
