package com.epam.spring.service;

import com.epam.spring.exception.EmptyTitleException;
import com.epam.spring.model.Recipe;
import com.epam.spring.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    public List<Recipe> getAll() {
        List<Recipe> all = recipeRepository.findAll();
        return all;
    }

    public void add(Recipe recipe) {
        if (recipe.getTitle().equals("")) {
            throw new EmptyTitleException("Title not entered");
        }
        recipeRepository.add(recipe);
    }

    public void delete(int id) {
        recipeRepository.delete(id);
    }
}
